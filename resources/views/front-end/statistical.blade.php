@extends('front-end.masterpage.master')
@section('content')
    <main>
        <div class="container">
            <section>
                <h2>Danh sách các hãng bay đạt doanh thu cao nhất</h2>
                <article>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h4><strong>Tên Hãng Bay</strong></h4>
                                            <div><span class="place"></span></div>
                                        </div>
                                        <div class="col-sm-3 text-right">
                                            <h4 ><strong>Tổng Doanh Thu</strong></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                @foreach($sql as $row)
                <article>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h4><strong><a href="">{{ $row->airways_name }}</a></strong></h4>
                                            <div><span class="place"></span></div>
                                        </div>
                                        <div class="col-sm-3 text-right">
                                            <h4 class="price text-danger"><strong> {{ number_format($row->airways_total_revanue) }} VNĐ</strong></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                @endforeach
                <div class="text-center">
                {{ $sql->links() }}
                </div>
            </section>
        </div>
    </main>
@stop